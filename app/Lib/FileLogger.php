<?php

namespace App\Lib;

class FileLogger implements ILogger
{
  
  private $file;
  
  /**
   * define file where log to be written into
   * @param $new_file - string with file name
   */
  public function __construct($new_file)
  {
    $this->file = $new_file;
  }
  
  
  /**
   * write data into event.log
   * @param $event - string with data to be inserted
   */
  //
  public function write($event)
  {
    
    //file handler = open file ($file)
    //'a' - Open for writing only, If the file does not exist, attempt to create it; add to the existing content 
    //'w' - Open for writing only, erase all content of the file, insert new data
    $fh = fopen($this->file, 'a'); 
    
    //write into file
    //fputs($fh, $event);//this will add all records in 1 line, hard to read by human
    fputs($fh, $event . PHP_EOL);//PHP_EOL - means end of line symbol, so next log record will start with a new line
    //fwrite($fh, $event . PHP_EOL);//same as fputs()
    fclose($fh);//close file
    
  }//END write()
  
  
}//END class FileLogger






















