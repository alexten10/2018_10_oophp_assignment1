<?php

namespace App\Lib;

//$dbh = new PDO ();

class DatabaseLogger implements ILogger
{
  
  private $dbh;
  
  
  /**
   * define db to work with
   * @param $new_dbh - new PDO
   */
  public function __construct($new_dbh)
  {
    $this->dbh = $new_dbh;
  }
  
  
  
  
  /**
   * write data($event) into DB table 'events'
   * @param $event - string data to be inserted
   */
  public function write($event)
  {
    $query = 'INSERT INTO events
              (event)
              VALUES
              (:event)
             ';
    $stmt = $this->dbh->prepare($query);
    $stmt->bindValue(':event', $event, \PDO::PARAM_STR);//because of namespace must \ before PDO
    //$stmt->bindValue(':event', $event);//can use without PDO::PARAM_STR
    
    if($stmt->execute()) {
      $id = $this->dbh->lastInsertId();
      $query = 'SELECT * FROM events WHERE id = :id';
      $stmt = $this->dbh->prepare($query);
      $stmt->bindValue(':id', $id, \PDO::PARAM_INT);
      //$stmt->bindValue(':id', $id);//can use without PDO::PARAM_STR
      $stmt->execute();
      
    }//END if
    
    
    
  }//END write()
  
  
}//END class DatabaseLogger






