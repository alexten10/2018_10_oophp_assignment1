<?php
/**
 * App Class
 * Front Controller Class for Assignment 1
 */

namespace App;

class App
{

	/**
	 * IOC container
	 */
	private $c;

	/**
	 * @param Pimple $container
	 */
	public function __construct(\Pimple\Container $container)
	{
		$this->c = $container;
	}

	/**
	 * Run the app
	 */
	public function run()
	{
		$c = $this->c;
		$this->log($c['logger']);
	}

	/**
	 * Log requests
	 *get info for logging and assign it to $event
	 */
	public function log(\App\Lib\ILogger $logger)
	{
		// do something to concat an $event string
		// Hint... most info can be found in $_SERVER
		
		
    
    //GET INFO FROM $_SERVER GLOBAL VARIABLE
    //"Y-m-d" - 2018-09-15; "Y/m/d" - 2018/09/15; etc
    //h-12hour; H-24hour; i-minutes 00 to 59; s-seconds00 to 59; a-am OR pm
    $date = date("Y-m-d h:i:sa");
    $user_ip = $_SERVER['REMOTE_ADDR'];
    $visited_page = $_SERVER['REQUEST_URI'];
    $request_method = $_SERVER['REQUEST_METHOD'];// GET or POST
    $curr_response_code = http_response_code();
    
    //get user agent(browser name only)
    $arr_browsers = ["Firefox", "Chrome", "Safari", "Opera", "MSIE", "Edge"];
    $agent = $_SERVER['HTTP_USER_AGENT'];
    $user_browser = '';
    foreach ($arr_browsers as $browser) {
      if (strpos($agent, $browser) !== false) {
        $user_browser = $browser;
        break;
      }//END if
    }//END foreach
    
    //ASSIGN INFO TO $event
    //$event = $date . ' | ' . $user_ip . ' | ' . $user_browser . ' | ' . $request_method . ' | ' . $curr_response_code . ' | ' . $visited_page;
    
    //same as above looooong line of code but devided into short concatinated segments
    $event = $date . ' | ' . $user_ip . ' | ' . $user_browser . ' | ';
    $event = $event . $request_method . ' | ' . $curr_response_code . ' | ';
    $event = $event . $visited_page;

		/* DO NOT EDIT BELOW THIS LINE
		--------------------------------------------------- */

		echo "<p>Logged using " . get_class($logger) . ":</p>";
		var_dump($event);
		$logger->write($event);
		
	}


}
